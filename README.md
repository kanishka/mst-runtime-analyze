# mst-runtime-analyze

This tool allows people curious about minimum spanning tree (mst) algorithm
performance to explore its runtime behavior on graphs of different sizes
and shapes. The tool also provides a basic visualization of a minimum 
spanning tree. 

## Usage

### compare

This mode allows you to compare the algorithm's performance on three
randomly generated graphs. You provide the three options for each randomly
generated graph as command line arguments, and then the tool presents
the runtime performance for running the mst algorithm on each graph.

The three options are:

* Number of nodes.
* Approximate percent of edges that are created, where 100% means all possible
edges are created.
* The name of the distribution to use when generating the weight for a given
edge. The current allowed options are `uniform` and `lopsided`.

Here is an example run:

```
$ go run . compare 100 0.10 uniform 1000 0.10 uniform 10000 0.10 uniform
100 nodes, 0.1 connected, uniform: 0ms
1000 nodes, 0.1 connected, uniform: 3ms
10000 nodes, 0.1 connected, uniform: 204ms
```

### solve 

This mode allows you to run the algorithm on one random graph and visualize
the solution. It takes the same option values as the `compare` mode, except it
only generates and solves one graph. Once complete, the solution and generated
graph are encoded in the DOT format, which you can visualize using graphviz. 
One quick online tool that runs graphviz is located [here](https://dreampuf.github.io/GraphvizOnline/).


Here is an example run:

```
$ go run . solve 5 .75 uniform
 strict graph G { 
   0 -- 2 [len= 8 ]
   0 -- 3 [len= 5 , color=green]
   0 -- 4 [len= 8 ]
   0 -- 1 [len= 7 ]
   1 -- 2 [len= 1 , color=green]
   1 -- 4 [len= 3 , color=green]
   2 -- 3 [len= 6 , color=green]
   3 -- 4 [len= 8 ]
 }
```

Here is a snapshot of the graphviz visualization of that graph:

![sample solution](https://kanishka-archive.codeberg.page/graphviz.svg)